package ch.ffhs.ftoop.schupplibachmann;

public class Zaehler extends Thread {

	private Speicher speicher;
	private int max, min;

	/**
	 * 
	 * @param s
	 *            Das Speicherobject, das die aktuelle Zahl haelt.
	 * @param min
	 *            Der Startwert f�r den Zaehler
	 * @param max
	 *            Der Endwert f�r den Zaehler (einschliesslich)
	 * 
	 */
	Zaehler(Speicher s, int min, int max) {
		this.speicher = s;
		this.max = max;
		this.min = min;
	}

	/**
	 * Diese Run Methode zählt den Wert in Speicher hoch - von min bis max
	 * (einschliesslich).
	 * 
	 */
	@Override
	public void run() {
		try {
			// checke ob min/max vertauscht werden muss
			if (this.min > this.max)
			{
				int i = this.max;
				this.max = this.min;
				this.min = i;
			}
			// setze den minimum Wert
			speicher.setWert(this.min);

			// ist minimum Wert und maximum Wert gleich?
			if (this.min == this.max)
				// dann sind wir fertig
				return;

			do {
				//wurde der gesetzte Wert konsumiert?
				if (!speicher.isHatWert())
				{
					// dann setzt denn nächsten Wert (increment/decrement)
					speicher.setWert(++this.min);
				}
				// warte für 10 Millisekunden
				Thread.sleep(10);
			}
			// mach das, solange wir nicht das Maximum erreicht haben
			while (this.min < this.max);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
