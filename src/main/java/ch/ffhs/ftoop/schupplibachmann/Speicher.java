package ch.ffhs.ftoop.schupplibachmann;

public class Speicher implements SpeicherIf {

	private int wert;
	private boolean hatWert = false;

	/**
	 * Gibt den aktuellen Wert zurück.
	 * @return Den aktuellen Wert im Speicher
	 * @throws InterruptedException
	 */
	public int getWert() throws InterruptedException {
		this.hatWert = false;
		return wert;
	}

	/**
	 * Setzt einen neuen aktuellen Wert.
	 * @param wert Der neu zu setzende Wert
	 * @throws InterruptedException
	 */
	public void setWert(int wert) throws InterruptedException {
		this.wert = wert;
		this.hatWert = true;
	}

	/**
	 * Gibt true zurück, wenn es einen neuen, noch nicht konsumierten Wert im
	 * Objekt hat.
	 * @return True wenn ein nicht konsumierter Wert im Objekt ist
	 */
	public boolean isHatWert() {
		return hatWert;
	}
}
