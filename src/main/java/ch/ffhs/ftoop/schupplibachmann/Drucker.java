package ch.ffhs.ftoop.schupplibachmann;

public class Drucker extends Thread {
	private Speicher speicher;

	Drucker(Speicher s) {
		this.speicher = s;
	}

	/**
	 * Holt einen Wert vom Zaehler und gibt ihn aus, gefolgt von einem einzelnen
	 * Leerzeichen.
	 * 
	 */
	@Override
	public void run() {
		while (true) {
			try {
				// gibt es einen neuen Wert im Speicher?
				if (speicher.isHatWert())
					// dann schreib ihn auf die Konsole
					System.out.print(speicher.getWert() + " ");
				// warte für 10 Millisekunden
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
