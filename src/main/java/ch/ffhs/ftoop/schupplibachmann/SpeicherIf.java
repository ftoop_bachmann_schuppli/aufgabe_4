package ch.ffhs.ftoop.schupplibachmann;

public interface SpeicherIf {

	/**
	 * Gibt den aktuellen Wert zurück.
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	int getWert() throws InterruptedException;

	/**
	 * Setzt einen neuen aktuellen Wert.
	 * 
	 * @param wert
	 * @throws InterruptedException
	 */
	void setWert(int wert) throws InterruptedException;

	/**
	 * Gibt true zurück, wenn es einen neuen, noch nicht konsumierten Wert im
	 * Objekt hat.
	 * 
	 * @return
	 */
	boolean isHatWert();

}
